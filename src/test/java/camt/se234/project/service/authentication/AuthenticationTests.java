package camt.se234.project.service.authentication;

import camt.se234.project.dao.UserDao;
import camt.se234.project.entity.User;
import camt.se234.project.repository.UserRepository;
import camt.se234.project.service.AuthenticationService;
import camt.se234.project.service.AuthenticationServiceImpl;
import org.hamcrest.Matcher;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.Mock;
import org.mockito.Mockito;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.autoconfigure.security.SecurityProperties;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.context.junit4.SpringRunner;

import javax.transaction.Transactional;
import java.util.List;

import static org.hamcrest.MatcherAssert.assertThat;
import static org.hamcrest.core.Is.is;
import static org.hamcrest.core.IsNull.notNullValue;
import static org.hamcrest.core.IsNull.nullValue;

public class AuthenticationTests {

    @Test
    public void testAuthenticatePass() {
        UserDao userDao = Mockito.mock(UserDao.class);
        User mockUser = User.builder().id(1L).username("user").password("password").role("user").build();
        Mockito.when(userDao.getUser("user", "password")).thenReturn(mockUser);

        AuthenticationServiceImpl service = new AuthenticationServiceImpl();
        service.setUserDao(userDao);

        User user = service.authenticate("user", "password");

        assertThat(user.getId(), is(mockUser.getId()));
        assertThat(user.getRole(), is(mockUser.getRole()));
    }

    @Test
    public void testAuthenticateWithWrongPassword() {
        UserDao userDao = Mockito.mock(UserDao.class);
        User mockUser = User.builder().id(1L).username("user").password("password").role("user").build();
        Mockito.when(userDao.getUser("user", "password")).thenReturn(mockUser);

        AuthenticationServiceImpl service = new AuthenticationServiceImpl();
        service.setUserDao(userDao);

        User user = service.authenticate("user", "wrongPassword");

        assertThat(user, is(nullValue()));
    }

    @Test
    public void testAuthenticateWithWrongUsername() {
        UserDao userDao = Mockito.mock(UserDao.class);
        User mockUser = User.builder().id(1L).username("user").password("password").role("user").build();
        Mockito.when(userDao.getUser("user", "password")).thenReturn(mockUser);

        AuthenticationServiceImpl service = new AuthenticationServiceImpl();
        service.setUserDao(userDao);

        User user = service.authenticate("wrongUser", "password");

        assertThat(user, is(nullValue()));
    }

    @Test
    public void testAuthenticateWithEmptyInput() {
        UserDao userDao = Mockito.mock(UserDao.class);
        User mockUser = User.builder().id(1L).username("user").password("password").role("user").build();
        Mockito.when(userDao.getUser("user", "password")).thenReturn(mockUser);

        AuthenticationServiceImpl service = new AuthenticationServiceImpl();
        service.setUserDao(userDao);

        User user = service.authenticate("", "");

        assertThat(user, is(nullValue()));
    }

}
