package camt.se234.project.service.project;

import camt.se234.project.dao.ProductDao;
import camt.se234.project.entity.Product;
import camt.se234.project.service.ProductService;
import camt.se234.project.service.ProductServiceImpl;
import com.sun.org.apache.xerces.internal.util.PropertyState;
import org.hamcrest.core.Is;
import org.junit.Test;

import java.util.ArrayList;
import java.util.List;

import static com.sun.org.apache.xerces.internal.util.PropertyState.is;
import static org.hamcrest.MatcherAssert.assertThat;
import static org.mockito.Mockito.mock;
import static org.mockito.Mockito.when;

public class GetAllProjectsTests {

    @Test
    public void testGetAllProducts() {
        ProductDao dao = mock(ProductDao.class);
        ProductServiceImpl service = new ProductServiceImpl();
        service.setProductDao(dao);
        List<Product> mockProducts = new ArrayList<>();
        mockProducts.add(Product.builder().id(1L).productId("p1").name("product1").price(10d).description("product1").build());
        mockProducts.add(Product.builder().id(2L).productId("p2").name("product2").price(20d).description("product2").build());
        mockProducts.add(Product.builder().id(3L).productId("p3").name("product3").price(30d).description("product3").build());
        mockProducts.add(Product.builder().id(4L).productId("p4").name("product4").price(40d).description("product4").build());

        when(dao.getProducts()).thenReturn(mockProducts);

        List<Product> products = service.getAllProducts();

        assertThat(products, Is.is(mockProducts));
    }

    @Test
    public void testGetAllProductsWithReturnEmpty() {
        ProductDao dao = mock(ProductDao.class);
        ProductServiceImpl service = new ProductServiceImpl();
        service.setProductDao(dao);
        List<Product> mockProducts = new ArrayList<>();

        when(dao.getProducts()).thenReturn(mockProducts);

        List<Product> products = service.getAllProducts();

        assertThat(products, Is.is(mockProducts));
    }
}
