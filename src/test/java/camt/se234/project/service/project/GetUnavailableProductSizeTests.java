package camt.se234.project.service.project;

import camt.se234.project.dao.ProductDao;
import camt.se234.project.entity.Product;
import camt.se234.project.service.ProductServiceImpl;
import org.hamcrest.Matchers;
import org.junit.Test;

import java.util.ArrayList;
import java.util.List;

import static org.hamcrest.MatcherAssert.assertThat;
import static org.mockito.Mockito.mock;
import static org.mockito.Mockito.when;

public class GetUnavailableProductSizeTests {

    @Test
    public void testGetUnavailableProductsWithAllAvailable() {
        ProductDao dao = mock(ProductDao.class);
        ProductServiceImpl service = new ProductServiceImpl();
        service.setProductDao(dao);

        List<Product> mockProducts = new ArrayList<>();
        Product product1 = Product.builder().id(1L).productId("p1").name("product1").price(10d).description("product1").build();
        mockProducts.add(product1);
        Product product2 = Product.builder().id(2L).productId("p2").name("product2").price(20d).description("product2").build();
        mockProducts.add(product2);
        Product product3 = Product.builder().id(3L).productId("p3").name("product3").price(30d).description("product3").build();
        mockProducts.add(product3);
        when(dao.getProducts()).thenReturn(mockProducts);

        int size = service.getUnavailableProductSize();

        assertThat(size, Matchers.is(0));
    }

    @Test
    public void testGetUnavailableProductsWithPartAvailable() {
        ProductDao dao = mock(ProductDao.class);
        ProductServiceImpl service = new ProductServiceImpl();
        service.setProductDao(dao);

        List<Product> mockProducts = new ArrayList<>();
        Product product1 = Product.builder().id(1L).productId("p1").name("product1").price(0d).description("product1").build();
        mockProducts.add(product1);
        Product product2 = Product.builder().id(2L).productId("p2").name("product2").price(-2d).description("product2").build();
        mockProducts.add(product2);
        Product product3 = Product.builder().id(3L).productId("p3").name("product3").price(2d).description("product3").build();
        mockProducts.add(product3);

        when(dao.getProducts()).thenReturn(mockProducts);

        int size = service.getUnavailableProductSize();

        assertThat(size, Matchers.is(2));
    }

    @Test
    public void testGetUnavailableProductsWithAllUnavailable() {
        ProductDao dao = mock(ProductDao.class);
        ProductServiceImpl service = new ProductServiceImpl();
        service.setProductDao(dao);

        List<Product> mockProducts = new ArrayList<>();
        Product product1 = Product.builder().id(1L).productId("p1").name("product1").price(0d).description("product1").build();
        mockProducts.add(product1);
        Product product2 = Product.builder().id(2L).productId("p2").name("product2").price(-1d).description("product2").build();
        mockProducts.add(product2);
        Product product3 = Product.builder().id(3L).productId("p3").name("product3").price(-2d).description("product3").build();
        mockProducts.add(product3);

        when(dao.getProducts()).thenReturn(mockProducts);

        int size = service.getUnavailableProductSize();

        assertThat(size, Matchers.is(3));
    }
}
