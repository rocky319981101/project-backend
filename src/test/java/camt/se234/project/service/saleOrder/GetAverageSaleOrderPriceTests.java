package camt.se234.project.service.saleOrder;

import camt.se234.project.dao.OrderDao;
import camt.se234.project.dao.ProductDao;
import camt.se234.project.entity.Product;
import camt.se234.project.entity.SaleOrder;
import camt.se234.project.entity.SaleTransaction;
import camt.se234.project.service.ProductServiceImpl;
import camt.se234.project.service.SaleOrderServiceImpl;
import org.hamcrest.Matchers;
import org.junit.Assert;
import org.junit.Test;

import java.util.ArrayList;
import java.util.List;

import static org.hamcrest.MatcherAssert.assertThat;
import static org.hamcrest.core.Is.is;
import static org.mockito.Matchers.isNotNull;
import static org.mockito.Matchers.same;
import static org.mockito.Mockito.mock;
import static org.mockito.Mockito.when;

public class GetAverageSaleOrderPriceTests {

    @Test
    public void testGetAverageSaleOrderPriceWithSingleProduct() {

        SaleOrder order = SaleOrder.builder()
                .saleOrderId("S001")
                .build();
        Product p1 = Product.builder().productId("P001").price(12d).name("product1").build();
        List<SaleTransaction> transactions = new ArrayList<>();
        transactions.add(SaleTransaction.builder().transactionId("t0001")
                .product(p1)
                .amount(10)
                .build());
        order.setTransactions(transactions);
        List<SaleOrder> orders = new ArrayList<>();
        orders.add(order);

        OrderDao dao = mock(OrderDao.class);
        SaleOrderServiceImpl service = new SaleOrderServiceImpl();
        service.setOrderDao(dao);
        when(dao.getOrders()).thenReturn(orders);
        double price = service.getAverageSaleOrderPrice();
        assertThat(price, Matchers.closeTo(12d * 10, 0.0001));
    }

    @Test
    public void testGetAverageSaleOrderPriceWithMultiProducts() {
        SaleOrder order = SaleOrder.builder()
                .saleOrderId("S001")
                .build();
        Product p1 = Product.builder().productId("P001").price(12d).name("product1").build();
        Product p2 = Product.builder().productId("P002").price(20d).name("product2").build();
        List<SaleTransaction> transactions = new ArrayList<>();
        transactions.add(SaleTransaction.builder().transactionId("t0001").product(p1).amount(10).build());
        transactions.add(SaleTransaction.builder().transactionId("t0002").product(p2).amount(5).build());
        order.setTransactions(transactions);
        List<SaleOrder> orders = new ArrayList<>();
        orders.add(order);

        OrderDao dao = mock(OrderDao.class);
        SaleOrderServiceImpl service = new SaleOrderServiceImpl();
        service.setOrderDao(dao);
        when(dao.getOrders()).thenReturn(orders);
        double price = service.getAverageSaleOrderPrice();
        assertThat(price, Matchers.closeTo(12d * 10 + 20d * 5, 0.0001));
    }

    @Test
    public void testGetAverageSaleOrdersPriceWithSingleProduct() {
        Product p1 = Product.builder().productId("P001").price(12d).name("product1").build();
        Product p2 = Product.builder().productId("P002").price(20d).name("product2").build();
        List<SaleOrder> orders = new ArrayList<>();

        SaleOrder order1 = SaleOrder.builder()
                .saleOrderId("S001")
                .build();
        List<SaleTransaction> transactions1 = new ArrayList<>();
        transactions1.add(SaleTransaction.builder().transactionId("t0001").product(p1).amount(10).build());
        order1.setTransactions(transactions1);
        orders.add(order1);

        SaleOrder order2 = SaleOrder.builder()
                .saleOrderId("S001")
                .build();
        List<SaleTransaction> transactions2 = new ArrayList<>();
        transactions2.add(SaleTransaction.builder().transactionId("t0002").product(p2).amount(5).build());
        order2.setTransactions(transactions2);
        orders.add(order2);

        OrderDao dao = mock(OrderDao.class);
        SaleOrderServiceImpl service = new SaleOrderServiceImpl();
        service.setOrderDao(dao);
        when(dao.getOrders()).thenReturn(orders);
        double price = service.getAverageSaleOrderPrice();
        assertThat(price, Matchers.closeTo((12d * 10 + 20d * 5) / 2, 0.0001));
    }

    @Test
    public void testGetAverageSaleOrdersPriceWithMultiProducts() {
        Product p1 = Product.builder().productId("P001").price(12d).name("product1").build();
        Product p2 = Product.builder().productId("P002").price(20d).name("product2").build();
        List<SaleOrder> orders = new ArrayList<>();

        SaleOrder order1 = SaleOrder.builder()
                .saleOrderId("S001")
                .build();
        List<SaleTransaction> transactions1 = new ArrayList<>();
        transactions1.add(SaleTransaction.builder().transactionId("t0001").product(p1).amount(10).build());
        transactions1.add(SaleTransaction.builder().transactionId("t0002").product(p2).amount(5).build());
        order1.setTransactions(transactions1);
        orders.add(order1);

        SaleOrder order2 = SaleOrder.builder()
                .saleOrderId("S001")
                .build();
        List<SaleTransaction> transactions2 = new ArrayList<>();
        transactions1.add(SaleTransaction.builder().transactionId("t0001").product(p1).amount(8).build());
        transactions2.add(SaleTransaction.builder().transactionId("t0002").product(p2).amount(6).build());
        order2.setTransactions(transactions2);
        orders.add(order2);

        OrderDao dao = mock(OrderDao.class);
        SaleOrderServiceImpl service = new SaleOrderServiceImpl();
        service.setOrderDao(dao);
        when(dao.getOrders()).thenReturn(orders);
        double price = service.getAverageSaleOrderPrice();
        assertThat(price, Matchers.closeTo((12d * 10 + 20d * 5 + 12d * 8 + 20d * 6) / 2, 0.0001));
    }
}
