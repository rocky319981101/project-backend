package camt.se234.project.service.saleOrder;

import camt.se234.project.dao.OrderDao;
import camt.se234.project.dao.ProductDao;
import camt.se234.project.entity.Product;
import camt.se234.project.entity.SaleOrder;
import camt.se234.project.entity.SaleTransaction;
import camt.se234.project.service.ProductServiceImpl;
import camt.se234.project.service.SaleOrderServiceImpl;
import org.hamcrest.Matchers;
import org.junit.Assert;
import org.junit.Test;

import java.util.ArrayList;
import java.util.List;

import static org.hamcrest.MatcherAssert.assertThat;
import static org.hamcrest.core.Is.is;
import static org.mockito.Matchers.isNotNull;
import static org.mockito.Matchers.same;
import static org.mockito.Mockito.mock;
import static org.mockito.Mockito.when;

public class GetSaleOrdersTests {

    @Test
    public void testGetSaleOrders() {
        OrderDao dao = mock(OrderDao.class);
        SaleOrderServiceImpl service = new SaleOrderServiceImpl();
        service.setOrderDao(dao);

        List<SaleOrder> mockOrders = new ArrayList<>();
        List<SaleTransaction> transactions = new ArrayList<>();
        transactions.add(SaleTransaction.builder().transactionId("t0001").product(new Product()).amount(10).build());
        transactions.add(SaleTransaction.builder().transactionId("t0002").product(new Product()).amount(12).build());
        SaleOrder order = SaleOrder.builder().saleOrderId("s01").id(1L).transactions(transactions).build();
        mockOrders.add(order);
        mockOrders.add(SaleOrder.builder().saleOrderId("s02").id(2L).build());

        when(dao.getOrders()).thenReturn(mockOrders);

        List<SaleOrder> orders = service.getSaleOrders();

        assertThat(orders, is(mockOrders));
        assertThat(orders.size(), is(mockOrders.size()));
    }

    @Test
    public void testGetSaleOrdersWithReturnEmpty() {
        OrderDao dao = mock(OrderDao.class);
        SaleOrderServiceImpl service = new SaleOrderServiceImpl();
        service.setOrderDao(dao);

        List<SaleOrder> mockOrders = new ArrayList<>();

        when(dao.getOrders()).thenReturn(mockOrders);

        List<SaleOrder> orders = service.getSaleOrders();

        assertThat(orders, is(mockOrders));
        assertThat(orders.size(), is(mockOrders.size()));
    }
}
